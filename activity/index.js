/*
Activity:
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.

*/

// Answer number 1:
let number1 = "8";

message = `The cube of 2 is ${number1}`;
console.log(`The cube of 2 is ${number1}`)

// Answer:
let street = "258 washington Ave NW";
let city = "California 900011";

message = `I live at ${street} ${city}`;
console.log(`I live at ${street} ${city}`)







// Asnwer number 2:
const person = {
    givenName: "Lolong",
    placeName: "saltwater crocodile",
    familyName: "1075 kgs"
    
};

const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);


console.log(`${givenName} was a ${maidenName}. He weighed at ${familyName} with a measurement of 20 ft 3 in.`);


// Answer number 3:

const numbers = ["1", "2", "3", "4", "5", "15"];

numbers.forEach(function(numbers){
	console.log(`${numbers}`);
})







// Answer number 4:
const reduceNumber = numbers.reduce((number1, next) => number1 + next, 0);
console.log(reduceNumber);


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
}
const myDog = new Dog();
myDog.name = "Brownie";
myDog.age = 10;
myDog.breed = "Labrador";
console.log(myDog);