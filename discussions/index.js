// JAVASCRIPT ES6 Updates.

// I.
// [SECTION] Exponent Operator

// ES5 Version
/*
- We used a double multiply symbol for this one.
*/ 
const firstNum = 8  ** 2;
console.log(firstNum);

// ES6 Updates
/*
- Math.pow mas organize.
- mahaba yung Math.pow
- pow means "to the power of"
- Pars float???

*/
const secondNum = Math.pow(8, 2);
console.log(secondNum);

/*----------------------------------------------*/

// II.
// [SECTION] Template Literals
/*
	- Allows to write string without using the concatenation operator (+)
	- Greatly helps with code readability.
*/


// A.
// for example:
let name = "John";

// pre-Template literals string
// ES5 version
let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Message without template literals:" + message);


// B.
// String Using Template Literals
// Uses backticks (``)
// backticks means ...
// Instead of using the + symbol, we used `` symbol for less hassle.

message = `Hello ${name}! Welcome to programming!`;
console.log(`Message without template ${message}`)

// puro lang ito syntax, walang logic.
// makisama kayo sa inyong Señior nyo kung anong gamit nila sa Syntax.


/*
----------------------------------------
*/

// III.
// Multi-line Using Template Literals

/*
// Instead of using the + symbol, we used `` symbol for less hassle.

// for example:
const myName = `My name is ${myName}`

*/
const anotherMessage = `
${name} attended a math competition.
He won it by solving a problem 8 ** 2 with the solution of ${firstNum}.
`

console.log(anotherMessage);


// another example:
const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

/*------------------------------------------*/

// IV.

// [SECTION] Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variable.
	-Allows us to name array elements with variables instead of using index numbers.
	- Helps with code readability.

	SYNTAX:

		let/const [variableName1, variableName2, variableName3] = arrayName
*/

const fullName = ["Juan", "Dela", "Cruz"];


// A.
// Pre-Array Destructuring - ES5
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! IT's nice to meet you. `)



// B.
// Array Destructuring - ES6
/*
- Ito my destructuring na mangyayari.
- wala siyang index sa loob ng console.log
- para rin siyang "array element renaming"
- kung ayaw mo mag indexes, ito gamitin mo.
*/
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)


/*---------------------------------------------*/
// Same lng ito sa array. same lng ng logic sa array.

// V.

// [SECTION] Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects.
	
	SYNTAX:
		let/const {propertyName, propertyName, propertyName} = object;

*/

// for example:
const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
};


// A.
// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);


// B.
// Object Destructuring
const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName ({ givenName, maidenName, familyName}) {
    console.log(`${ givenName } ${ maidenName } ${ familyName }`);
}

getFullName(person);

/*
Q: pwede yan kahit ilang properties ilagay sir?
Ans: YES   
*/

/*---------------------------------------------*/

// VI.
// [SECTION] Arrow Functions
/*

=> means arrow functions.
	- Compact alternate syntax to traditional functions.
	- Useful for code snippets where creating functions will not be reused in any other potion of the code.
	- "DRY" (Don't Repeat Yourself) means principle where's no longer need to create a function and think of a name for functions that will only be used in certain code snippets.

	SYNTAX:
		const variableName = () => {
			console.log()
		}
*/

// for example:
const hello = () => {
	console.log("Hello World");
}

hello();

/*

=> means arrow functions.

*/

/*---------------------------------------------*/

// VII.

// A.
// Pre-Arrow Function and Template Literals
/*
	- Syntax
		function functionName(parameterA, parameterB, parameterC) {
			console.log();
		}
*/

function printFullName (firstName, middleInitial, lastName) {
    console.log(firstName + ' ' + middleInitial + '. ' + lastName);
}

printFullName("John", "D", "Smith");



// B.
// Arrow Function
/*
	SYNTAX:

		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log();
		}
*/


// ES5 Functions
function printFullName (firstName, middleInitial, lastName) {
    console.log(firstName + ' ' + middleInitial + '. ' + lastName);
}

printFullName("John", "D", "Smith");




// ES6 Arrow functions
const printName = (first, middle, last) => {
	console.log(`${first} ${middle} ${last}`)
}

printName("John", "D", "George");

// Prone to bug ang semi-colon symbol.
/*

=> means arrow function.

	Doon sa ES5 gumamit tayo ng function.
	Sa ES6 naman imbes na function yung gagamitin naton, yung => arrow function yung ginagamit natin.

*/

/*----------------------------------------*/

// Arrow Functions with loops
// Pre-Arrow Function

const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student.`);
})



// Arrow Function
// The function is only used in the "forEach" method to print out a text with the student's names
students.forEach((student) => {
	console.log(`${student} is a student.`);
})


/*----------------------------------*/

// VIII.
// [SECTION] Implicit Return Stament
/*
	- There are instances when you omit the "return" statement.
	- This works becuase even without the "return" JavaScript implicitly adds it for the result of the function.

	SYNTAX:
	// Pre-Arrow Function

	const add = (x,y) => {
	return x + y;
	}
*/

const add = (x, y) => x + y;

let total = add(1,2);
console.log(total);
// Dapat walang {} curly braces dito.
// koni,condsider ni Javascript kahit walang {} curly braces or ; 
// pwede lagyan ng return statement
/*
		=> return = {x + y};

*/


/*--------------------------------------*/

// IX.
// [SECTION] Default Function Argument Value - ES6
/* 
 - Provides a default argument value if none is provided when the function is invoked.
*/

const greet = (name = 'Username') => {
	return `Good Morning, ${name}!`;
}

console.log(greet());

// pwede ka naman mag lagay ng (Juvix) sa loob  ng console.log(greet());
// if kung wala, babalik siya sa default na name na parameter.

/*----------------------------------------*/

// X.
// [SECTION] Class-Based Object Blueprints
/*
	- Allows creation/instantiation of objects using classes as blueprints
*/

// Creating a class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class.
	- The "this" keyword refers to the properties of an object created/initialized from the class
	- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
	- Syntax
		class className {
			constructor(objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

/*
	ES5 
	function Pokemon(name,level) {

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;
		}
*/


// ES6
class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
// Pwede tayo gumamit ng class at objects.



// Instantiating an object
/*
	- The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties
	- No arguments provided will create an object without any values assigned to it's properties
	- let/const variableName = new ClassName();
*/
// let myCar = new Car();

/*
	- Creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't re-assign it with another data type
	- It does not mean that it's properties cannot be changed/immutable
*/
const myCar = new Car();

console.log(myCar);

// Values of properties may be assigned after creation/instantiation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

// Creating/instantiating a new object from the car class with initialized values
const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);
// sa may const na myCar hindi pwede palitan. Yung sa loob lang ng console.log() ang pwedeng palitan.






// if kung hindi ka maglalagay ng new statement, mag,lalagay nalang kayo ng return statement.

// myCar ay hindi pwede ninyo baguhin, pwede yung mga properties such as:
// 			this.brand = brand;
// 			this.name = name;
// 			this.year = year;
// pwede ito baguhin, itong properties.

/*
Q: pag may nakita po tayong class, matic about object na po yung laman niya?
Ans: Yes
Mas versatile c function, si class is more on specific.
*/

// sa ES6 dapat walang concatination na + plus symbol. Instead, you need to used backticks `` at sa loob ng backticks ay ${}
// sa ES6 ang + plus sign ay tinatranslate into ${}
// sa loob ng curly braces, lagyan natin ng pangalan ba ng array or yung sa loob ng ().